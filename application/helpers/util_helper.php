<?php 
function version(){
	date_default_timezone_set('America/Lima');
	echo "?v=".date("YmdHis");	
}

function now() {
	date_default_timezone_set('America/Lima');
	return date("Y-m-d H:i:s");
}

function ver($text) {
	if (!isset($text) || empty($text))
		return '';
	echo '<pre>';
	var_dump($text);
	echo '</pre>';
}

function verp($text) {
	if (!isset($text) || empty($text))
		return '';
	echo '<pre>';
	print_r($text);
	echo '</pre>';
}


function mostrarVista($body, $data = false){
	$CI = & get_instance();
	echo $CI->load->view('template/v_header', $data, true);	
	echo $CI->load->view($body, $data, true);
	echo $CI->load->view('template/v_footer', $data, true);
}

function loadLibs($body, $data){
	$CI = & get_instance();
	// ob_start();
	echo '<div class="container">';
	echo $CI->load->view('template/v_header', $data, true);	
	echo $body;
	echo '</div>';
	echo $CI->load->view('template/v_footer', $data, true);
}

function perfil(){
	// $CI = & get_instance();
	// $CI->load->model('M_permisos');
	// return $CI->M_permisos->datosUser();
}


function agruparArray($array, $key) {
	$return = array();
	foreach($array as $k) {
		$v = (array)$k;
		$return[$v[$key]][] = $v;			
	}
	return $return;
}

function limpiarEstados($array, $key, $filtro, $llave) {
	$return = array();
	foreach($array as $k) {
		$v = (array)$k;
		if ($v[$filtro] == $llave) {
			$return[$v[$key]][] = $v;			
		}

	}
	return $return;
}


function moneda($value){
	$numero = round($value,2);
	return number_format($numero, 2);
}