<?php 

function enviarMail($para, $asunto, $mensaje, $CC = false, $CCO = false, $adjuntos = false){
	try {
		$CI = & get_instance();
		$config = Array(
			'mailtype'  => 'html', 
			'charset'   => 'utf-8'
		);
		$CI->load->library('email', $config);
		$CI->email->clear();
		if (is_array($adjuntos) && count($adjuntos)>0) {
			foreach ($adjuntos as $key => $value) {
				// $mail->AddAttachment('path_to_pdf', $name = 'Name_for_pdf',  $encoding = 'base64', $type = 'application/pdf');
				$CI->email->attach($value['archivo'], '', $value['nombre'], $value['type']);
				// $CI->email->attach($value, 'inline', null, '', true);
			}
		}
		if (is_array($CC) && count($CC)>0) {
			foreach ($CC as $key => $value) {
				$CI->email->cc($value);
			}
		}
		if (is_array($CCO) && count($CCO)>0) {
			foreach ($CCO as $key => $value) {
				$CI->email->bcc($value);
			}
		}

		if (is_array($para) && count($para)>0) {
			foreach ($para as $key => $value) {
				$CI->email->to($value);
			}
		}

		$CI->email->from('noreply@legrand.com.pe', 'Buzón Ético Legrand');
		$CI->email->reply_to('noreply@legrand.com.pe', 'Buzón Ético Legrand');
		$CI->email->subject($asunto);
		$CI->email->message($mensaje);
		if (!$CI->email->send()){
			$response['result'] 	= 'ERROR';
			$response['msj2'] 	= $CI->email->print_debugger();
			$response['msj']	= $CI->email->print_debugger(array('headers'));
		}else{
			$response['result'] 	= 'OK';
		}
		return $response;
	} catch (Exception $e) {
		throw new Exception($e);
		return;
	}

}

