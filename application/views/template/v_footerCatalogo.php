<footer class="footCatalogo">  
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <p class="text-center"> AV. JOSÉ PARDO 819 - MIRAFLORES</p>
                <br>
                <p class="text-center">CALLE JACINTO IBAÑEZ N°315,<br>MEGAECNTRO OFICINA D204,<br>PARQUE INDUSTRIAL - AREQUIPA</p>
            </div>
            <div class="col-md-3">
                <p class="text-center">ESCRIBENOS A: <br> <a href="mailto:contacto.peru@bticino.com" class="text-decoration-none">CONTACTO.PERU@BTCINO.COM</a></p>
            </div>
            <div class="col-md-3">
                <p class="text-center">
                    LLÁMANOS A: <br>6113-1800
                </p>
            </div>
            <div class="col-md-3">
            <p> ASISTENCIA TÉCNICA: <br> 0-800-17710</p>
            </div>
        </div>
    </div>
</footer>

</div>

<script src="<?php echo base_url('public/js'); ?>/util.js"></script>
</body>
</html>