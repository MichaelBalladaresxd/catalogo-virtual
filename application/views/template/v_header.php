<nav class="navbar navbar-expand-lg p-4 navbar-light">
	<div class="container">
		<a class="navbar-brand" href="<?php echo base_url('admin/') ?>index"><img src="<?php echo base_url('public/') ?>img/bticino.png" alt="" style="width: 120px"></a>
		<a class="navbar-brand mt-2 pl-4" href="<?php echo base_url('admin/') ?>index" id="legrandLogo"><img src="<?php echo base_url('public/') ?>img/legran.png" alt="" style="width: 120px"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link"  id="btnIniciarSesion" onclick="modalLogin()" role="button">Iniciar Sesion</a>
				</li>
				<li class="nav-item" >
					<a class="nav-link"  id="btnRegistro" onclick="modalRegistro()"  role="button">Registrarse</a>
				</li>

				<li class="nav-item ml-md-5">
					<a class="nav-link" href="<?php //echo base_url('articulos') ?>">Mi Lista</a>
				</li>
			</ul>
		</div>
	</div>
</nav>

