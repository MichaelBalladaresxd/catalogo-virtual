<div class="jumbotron bg-start mb-0">
    <h1 class="text-center text-white font-weight-bold display-3">Catálogo Virtual</h1>
    <p class="text-center text-white">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>


    <nav class="navbar navbar-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapseExample" aria-controls="collapseExample" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <p class="text-white">Menú de categorias</p>

        <div class="collapse" id="collapseExample">
            <div class="card card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
            </div>
        </div>

        <form class="form-inline col-md-10">
        <input class="form-control col-md-10" type="search" placeholder="Buscar en toda la página" aria-label="Search">
        <button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
        </form>
    </nav>

   
</div>

<div class="container-fluid">
    <div class="row">

        <div class="col-md-3">

            <ul class="nav flex-column bg-light p-3">
                <h5>Interruptores y Tomacorrientes</h5>
                <li class="nav-item">
                    <a class="nav-link active" href="#">Axolute</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Dominio Sencia</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Idrobox</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Living Light</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Living Now</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Magic</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Matix</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Modus Style</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Oval</a>
                </li>
            
            </ul>

            <ul class="nav flex-column bg-light p-3">
                <h5>TABLEROS Y PROTECCIONES ELÉCTRICAS</h5>
                <li class="nav-item">
                    <a class="nav-link active" href="#">Interruptor Diferencial</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Interruptor Termomágnetico</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Tableros eléctricos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Termomágnetico de engrampe</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Termomágnetico de tornillo</a>
                </li>
            </ul>

        </div>

        <div class="col-md-9">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                    
                        <li class="nav-item">
                            <a class="nav-link" href="#">Bticino > </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Categorias > </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Interruptores y tomacorrientes > </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Axolute</a>
                        </li>
                    
                    </ul>
                </div>

            </nav>
            
            
            <h1 class="text-center">AXOLUTE</h1>
            <h3 class="text-center">La máxima exxpresión en diseño, lujo y tecnología</h3>

            <div class="row my-3 catalogoCategorias"> 
                <div class="col-md-4">
                    <div class="card text-center mb-3">
                        <img src="<?php echo base_url('public/img/')?>interruptor.jpg" class="img-fluid  p-3">
                        <h5>INTERRUPTORES Y TOMACORRIENTESS</h5>
                        <p>Las extensiones electricas son una forma eficaz de llevar energia a dispositivos electricos [..]</p>

                        <button class="btn btn-primary">AGREGAR A MI LISTA -> </button>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card text-center mb-3">
                        <img src="<?php echo base_url('public/img/')?>interruptor.jpg" class="img-fluid  p-3">
                        <h4>INTERRUPTORES Y TOMACORRIENTESS</h4>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card text-center mb-3">
                        <img src="<?php echo base_url('public/img/')?>interruptor.jpg" class="img-fluid  p-3">
                        <h4>INTERRUPTORES Y TOMACORRIENTESS</h4>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card text-center mb-3">
                        <img src="<?php echo base_url('public/img/')?>interruptor.jpg" class="img-fluid  p-3">
                        <h4>INTERRUPTORES Y TOMACORRIENTESS</h4>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card text-center mb-3">
                        <img src="<?php echo base_url('public/img/')?>interruptor.jpg" class="img-fluid  p-3">
                        <h4>INTERRUPTORES Y TOMACORRIENTESS</h4>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card text-center mb-3">
                        <img src="<?php echo base_url('public/img/')?>interruptor.jpg" class="img-fluid  p-3">
                        <h4>INTERRUPTORES Y TOMACORRIENTESS</h4>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="card text-center mb-3">
                        <img src="<?php echo base_url('public/img/')?>interruptor.jpg" class="img-fluid  p-3">
                        <h4>INTERRUPTORES Y TOMACORRIENTESS</h4>
                    </div>
                </div>

            
            </div>

        </div>

    </div>

</div>

   

