<?php
// $log = $_SESSION['logged_in'];
// $logged = ($log==true) ? 'log' : 'notlog';
?>
<!-- <input type="hidden" id="statusLog" value=""> -->
<!-- <div class="col-12 bg-light pt-3 pb-3 text-left border-bottom ">
	<div class="container d-flex">
		<img src="<?php //echo base_url('public/') ?>img/logo-pucp.png" alt="" style="width: 120px">
		<div class="ml-auto">
			<a href="<?php //echo base_url('admin/') ?>logout" class="btn btn-danger mr-auto">Salir</a>
		</div>
	</div>
</div> -->

<div class="container-fluid">
	
	<div>
		<img src="<?php echo base_url('public/img/') ?>bg_catVirtual.png" class="img-fluid imgIndex"/> 
		<img src="<?php echo base_url('public/img/') ?>btcino-bg-2.png" class="img-fluid imgIndex"/> 
	</div>

	<div class="bg-start">
		<div class="row">
			<div class="bg-registro p-5 mx-auto">
				<form action="" class="frmRegistro">

					<h2 class="text-white text-center">REGISTRO</h2>

					<div class="form-row">
						<div class="col-md-6 mb-3">
							<input type="text" class="form-control" placeholder="Nombre" required>
						</div>
						<div class="col-md-6 mb-3">
							<input type="text" class="form-control" placeholder="Apellidos" required>
						</div>
					</div>

					<div class="form-row">
						<div class="col-md-6 mb-3">
							<input type="email" class="form-control" placeholder="Email" required>
						</div>
						<div class="col-md-6 mb-3">
							<input type="text" class="form-control" placeholder="N° Telf. de Contacto" required>
						</div>
					</div>

					<div class="form-row">
						<div class="col-md-6 mb-3">
							<input type="text" class="form-control" placeholder="Ocupacion" required>
						</div>
						<div class="col-md-6 mb-3">
							<input type="text" class="form-control" placeholder="N° DNI" required>
						</div>
					</div>

					<div class="form-row">
						<div class="col-md-6 mb-3">
							<input type="text" class="form-control" placeholder="N° de RUC (Si es Empresa) " >
						</div>
						<div class="col-md-6 mb-3">
							<input type="text" class="form-control" placeholder="Ciudad" >
						</div>
					</div>
					
					<div class="row">
						<button class="btn btn-primary text-white mx-auto" type="button">Enviar</button>
					</div>
					
					
				</form>
			</div>
		</div>
	</div>


	<div class="bg-start ">
		<div class="row">
			<div class="bg-registro p-5 mx-auto">
				<div class="frmIniciarSesion">
					<h2 class="text-center text-white"> Registro Completo</h2>
				</div>
				
			</div>
		</div>
	</div>


	<div class="bg-start img-fluid">
		<div class="row ">
			<div class="bg-registro p-5 mx-auto">
				<form action="" class="frmIniciarSesion">

					<h2 class="text-white text-center">Iniciar Sesion</h2>

					<div class="form-row">
						<div class="col-md-10 mx-auto mb-3">
							<input type="text" class="form-control" placeholder="Correo eléctronico" required>
						</div>
						
					</div>

					
					<div class="row">
						<button class="btn btn-primary text-white mx-auto" type="button">Iniciar</button>
					</div>
					
					
				</form>
			</div>
		</div>
	</div>
	
</div>

<script id="tmpFormRegistro" type="x-tmpl-mustache">
	<form id="frmRegistro">


		<div class="form-row">
			<div class="col-md-6 mb-3">
				<input type="text" class="form-control" name="txtNombre" placeholder="Nombre" required>
			</div>
			<div class="col-md-6 mb-3">
				<input type="text" class="form-control" name="txtApellido" placeholder="Apellidos" >
			</div>
		</div>

		<div class="form-row">
			<div class="col-md-6 mb-3">
				<input type="email" class="form-control" name="txtEmail" placeholder="Email" required>
			</div>
			<div class="col-md-6 mb-3">
				<input type="text" class="form-control" name="txtTelefono"  placeholder="N° Telf. de Contacto"  >
			</div>
		</div>

		<div class="form-row">
			<div class="col-md-6 mb-3">
				<input type="text" class="form-control" name="txtOcupacion" placeholder="Ocupacion" >
			</div>
			<div class="col-md-6 mb-3">
				<input type="text" class="form-control" name="txtDni" placeholder="N° DNI">
			</div>
		</div>

		<div class="form-row">
			<div class="col-md-6 mb-3">
				<input type="text" class="form-control" name="txtRuc" placeholder="N° de RUC (Si es Empresa) " >
			</div>
			<div class="col-md-6 mb-3">
				<input type="text" class="form-control" name="txtCiudad" placeholder="Ciudad" >
			</div>
		</div>

	</form>
</script>


<script id="tmpFormLogin" type="x-tmpl-mustache">
	<form id="frmLogin">


		<div class="form-row">
			<div class="col-md-10 mx-auto mb-3">
				<input type="text" class="form-control" placeholder="Correo eléctronico" name="email" required>
			</div>
			
		</div>


	</form>
</script>



<script id="tmpCompletoRegistro" type="x-tmpl-mustache">
	<div class="bg-registro p-5 mx-auto">
		<div class="frmIniciarSesion">
			<h2 class="text-center text-white"> Registro Completo</h2>
		</div>
		
	</div>
</script>