<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');
	}

	public function index(){
		
			$data["plugins"] = [
				"validate" => $this->load->view('plugins/Validator', "", true)
			];
			$this->load->view('template/v_head', $data, FALSE);
			$this->load->view('template/v_header', $data, FALSE);
			$this->load->view('pages/v_admin', $data, FALSE);
			$this->load->view('template/v_foot', $data, FALSE);

		
	}

	public function login(){
		$email = $this->input->post('email');

		$where['email'] = $email;
		$response = $this->M_datos->get('usuarios',$where,'row');
		
		try {
			if ($response != null) {
			
				if ($email == $response->email) {
					$newdata = array(
						'nombre' => $response->nombre,
						'apellidos' => $response->apellidos,
						'email' => $response->email,
						'logged_in' => TRUE
					);
					$this->session->set_userdata($newdata);
	
					
					$data['result'] = 'OK';
					$data['msj'] = 'Bienvenido';
					//redirect('/admin/catalogo', 'refresh');
				}
				
			
			}else{
				$data['result'] = 'ERROR';
				$data['msj'] = 'Su Usuario no es correcto.';

			}

			echo json_encode($data);
		} catch (Exception $e) {
			throw new Exception($e);
			return $e->getMessage();
		}
		
		

	//	echo json_encode($data);
	}

	public function logout(){
		session_destroy();
		redirect(base_url('admin'),'refresh');
	}

	public function catalogo(){
		$data["plugins"] = [
			"validate" => $this->load->view('plugins/Validator', "", true)
		];
		$this->load->view('template/v_head', $data, FALSE);
		$this->load->view('template/v_header', $data, FALSE);
		$this->load->view('pages/v_catalogo', $data, FALSE);
		$this->load->view('template/v_footerCatalogo', $data, FALSE);
	}

	public function categoriaProducto(){
		$this->load->view('template/v_head', FALSE);
		$this->load->view('template/v_header', FALSE);
		$this->load->view('pages/v_categoriaProducto', FALSE);
		$this->load->view('template/v_footerCatalogo', FALSE);
	}
	
	public function registro(){
		$this->form_validation->set_rules('txtNombre', 'Nombre', 'required');
		$this->form_validation->set_rules('txtEmail', 'Email', 'required|is_unique[usuarios.email]');

		if ($this->form_validation->run() == TRUE)
		{
				$tabla = 'usuarios';

				$parametros = array(
					'nombre' => $this->input->post('txtNombre'),
					'apellidos' => $this->input->post('txtApellido'),
					'email' => $this->input->post('txtEmail'),
					'telefono' => $this->input->post('txtTelefono'),
					'ocupacion' => $this->input->post('txtOcupacion'),
					'dni' => $this->input->post('txtDni'),
					'ruc' => $this->input->post('txtRuc'),
					'ciudad' => $this->input->post('txtCiudad')
				);

				$response = $this->M_datos->insert($tabla,$parametros);
				if ($response) {
					$response['result']	= 'OK';
					$response['msj']	= 'Datos guardados correctamente.';
				} else {
					$response['result']	= 'ERROR';
					$response['msj']	= 'Error al registrar datos.';
				}
		}
		else
		{
				$response['result']	= 'ERROR';
				$response['msj']	= 'Ingresar datos requeridos.';
		}

		echo json_encode($response);
	}
}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */