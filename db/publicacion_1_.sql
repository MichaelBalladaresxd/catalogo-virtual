-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-11-2020 a las 00:35:37
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `idehpucp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicacion`
--

CREATE TABLE `publicacion` (
  `id_publicacion` int(11) NOT NULL,
  `titulo` text COLLATE utf8_spanish_ci NOT NULL,
  `autor` text COLLATE utf8_spanish_ci NOT NULL,
  `ano_publicacion` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `organizacion` text COLLATE utf8_spanish_ci NOT NULL,
  `sumilla` text COLLATE utf8_spanish_ci NOT NULL,
  `url_img` text COLLATE utf8_spanish_ci NOT NULL,
  `url_archivo` text COLLATE utf8_spanish_ci NOT NULL,
  `usu_creacion` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `fec_registro` date NOT NULL,
  `fecmod` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `publicacion`
--

INSERT INTO `publicacion` (`id_publicacion`, `titulo`, `autor`, `ano_publicacion`, `organizacion`, `sumilla`, `url_img`, `url_archivo`, `usu_creacion`, `fec_registro`, `fecmod`) VALUES
(2, 'Criterios básicos para un espacio de conmemoración de la violencia en el Perú', 'Félix Reátegui, Patricia Luque, Inés Martens', '2012', 'Misereor/IDEHPUCP', 'En el marco del proyecto Construcción Social de la Memoria auspiciado por MISEREOR,\r\nel presente documento ofrece un conjunto de nociones sobre lo que se espera de un espacio nacional dedicado a la memoria de la violencia y sobre lo que este debería ser para\r\nsatisfacer las expectativas y los derechos de las víctimas y para responder con justicia al\r\ndeber de hacer memoria que tienen la sociedad y el Estado peruanos. Estas nociones proceden de diálogos con organizaciones de víctimas y de poblaciones afectadas sostenidos\r\npor el Instituto de Democracia y Derechos Humanos de la Pontificia Universidad Católica\r\ndel Perú. Se pretende, así, contribuir al proceso de reflexión sobre el pasado violento y de\r\nreconocimiento de los derechos de las víctimas con una perspectiva de derechos humanos\r\ny de consolidación de la democracia.', '/recursos/upload/2020/11/violencia_del_peru.jpg', '', '', '2020-11-17', '2020-11-17 00:00:00'),
(3, 'Los sitios de la memoria. Procesos sociales de la conmemoración en el Perú', 'Félix Reátegui, Rafael Barrantes, Jesús Peña', '2010', 'IDEHPUCP/Konrad Adenauer Stiftung', 'En el marco del proyecto Construcción Social de la Memoria auspiciado por MISEREOR,\r\nel presente documento ofrece un conjunto de nociones sobre lo que se espera de un espacio nacional dedicado a la memoria de la violencia y sobre lo que este debería ser para\r\nsatisfacer las expectativas y los derechos de las víctimas y para responder con justicia al\r\ndeber de hacer memoria que tienen la sociedad y el Estado peruanos. Estas nociones proceden de diálogos con organizaciones de víctimas y de poblaciones afectadas sostenidos\r\npor el Instituto de Democracia y Derechos Humanos de la Pontificia Universidad Católica\r\ndel Perú. Se pretende, así, contribuir al proceso de reflexión sobre el pasado violento y de\r\nreconocimiento de los derechos de las víctimas con una perspectiva de derechos humanos\r\ny de consolidación de la democracia.', '/recursos/upload/2020/11/proces_sociales.jpg', '', '', '2020-11-17', '2020-11-17 00:00:00'),
(4, 'El Santuario de la Memoria La Hoyada, Ayacucho. El proceso de diálogo y negociación en la construcción de un espacio de memoria', 'Iris Jave', '2017', 'Departamento de Ciencias Sociales (Pontificia Universidad Católica del Perú)', 'El proceso para la construcción del Santuario de la Memoria de La Hoyada (Ayacucho) –un espacio dedicado a conmemorar a las víctimas del conflicto armado interno- se viene desarrollando a partir de eventos participativos que involucran, de un lado, a las organizaciones de víctimas y de derechos humanos, en particular la Asociación Nacional de Familiares de Secuestrados, Detenidos y Desaparecidos del Perú (ANFASEP); y, por otro, a las ONGs y funcionarios de Ayacucho y Lima. En el medio, se encuentra la población vecina al terreno de la Hoyada, población asentada en el lugar debido a procesos recientes de invasión. El 2014, luego de una serie de acciones y construcción de vínculos con actores políticos y sociales, las socias de la ANFASEP lograron que el Estado peruano –representado por el ministro de Justicia- entregara formalmente el terreno sobre el que se erige el Santuario de la Memoria. Este acto se convirtió en una política de Estado al reconocer el destino del terreno como una reparación simbólica, contenida en la Ley de Reparaciones. Este trabajo pretende aportar otra mirada a los estudios sobre memoria en el país tomando como referencia el protagonismo asumido por las organizaciones de víctimas en la demanda de sus derechos y su incursión en los procesos de incidencia y comunicación, construyendo espacios de deliberación pública en los temas de memoria.', '/recursos/upload/2020/11/memoria_hoyada.jpg', '', '', '2020-11-17', '2020-11-17 00:00:00'),
(6, 'Prueba de Titulo 1', 'Autor prueba 1', '2020', 'Org Prueba', 'Prueba Sumilla ok 3', '/recursos/upload/2020/11/contratoholigram-0012.jpg', '', '', '0000-00-00', '2020-11-18 16:02:26'),
(7, 'PRUEBA FIN final', 'AUTOR FIN 2', '2020', 'ORG FIN 2', 'SUMILLAAAA 2', '/recursos/upload/2020/11/scan-191015-0002.jpg', '', '', '0000-00-00', '2020-11-20 09:50:19'),
(8, 'NUEVA PRUEBA', 'NUEVA PRUEBA', '2020', 'NUEVA PRUEBA', 'NUEVA PRUEBA\r\nNUEVA PRUEBA\r\nNUEVA PRUEBANUEVA PRUEBANUEVA PRUEBA', '/recursos/upload/2020/11/screenshot2020-10-27-inicio-moda-king-peru-import.png', '', '', '0000-00-00', '0000-00-00 00:00:00'),
(9, 'prueba foto con doc', 'doc y img', '2020', 'org img y docs', 'org img y docsorg img y docsorg img y docsorg img y docs\r\norg img y docs\r\n\r\norg img y docs\r\norg img y docs\r\n\r\norg img y docs\r\norg img y docs', '/recursos/upload/2020/11/raguilar-sodexo.png', '/recursos/upload/2020/11/raguilar-sodexo1.png', '', '0000-00-00', '0000-00-00 00:00:00'),
(10, '345ED', 'RFSFASDF', 'ASDFASDF', 'ASDFASD', 'ASDFASDF', '/recursos/upload/2020/11/fspca.png', '/recursos/upload/2020/11/fspca1.png', '', '0000-00-00', '0000-00-00 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `publicacion`
--
ALTER TABLE `publicacion`
  ADD PRIMARY KEY (`id_publicacion`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `publicacion`
--
ALTER TABLE `publicacion`
  MODIFY `id_publicacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
