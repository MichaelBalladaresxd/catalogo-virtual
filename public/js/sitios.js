let configDatatable = {
	destroy: true,
	"processing": true,
	"serverSide": true,
	// deferRender:    true,
	'sServerMethod': "POST",
	'ajax': {
		'url': base_url+'sitios/listaSitios',
		type: "POST",
		data: function(d){
			return $.extend({}, d, {
				"nombre"	: $("#nombre").val(), 
				"tipo"		: $("#tipo").val(),
				"region"	: $("#region").val(),
				"general"	: $("#general").val()
			});
		},
		complete: function(xhr,status) {
			load('off');
		}
	},
	"order": [[ 0, "desc" ]],
	columns: [
	{
		title: 'Sitios',
		data: 'id_sitio_memoria',
		render: function(data, type, full, row, meta){
			let template = document.getElementById('tmpSitioDetalle').innerHTML;
			let rendered = Mustache.render(template, {
				id						: full.id_sitio_memoria,
				codigo					: full.codigo,
				nombre					: full.nombre,
				tipo					: full.tipo,
				anio_creacion			: full.anio_creacion,
				fuente					: full.fuente,
				region					: full.region,
				provincia				: full.provincia,
				distrito				: full.distrito,
				ubicacion_exacta		: full.ubicacion_exacta,
				ubicacion_exacta_maps	: isUrl(full.ubicacion_exacta_maps),
				ubicacion_iframe		: isUrl(full.ubicacion_iframe),
				institucion_creacion	: full.institucion_creacion,
				institucion_cargo		: full.institucion_cargo,
				contacto				: full.contacto,
				razon_creacion			: full.razon_creacion,
				descripcion_fisica		: full.descripcion_fisica,
				referencia_uso			: full.referencia_uso,
				estado_actual			: full.estado_actual,
				archivos_guardados		: full.archivos_guardados,
				sitio_web				: full.sitio_web,
				logged					: logged,
				foto 					: (full.img_ubicacion)?base_url+full.img_ubicacion:full.img_ubicacion,
				id 						: full.id_sitio_memoria
			});
			return rendered;
		}
	}]
}

if (logged) {
	configDatatable.buttons = ['New', 'reload'];
	configDatatable.dom 	= "<'row'<'col-sm-6'><'col-sm-6 text-right'B>><'row'<'col-sm-12'tr>><'row'<'col-sm'l><'col-sm'p>><'row'<'col-sm'i>>";
}

let dataTable = $('#tableResult').DataTable(configDatatable).on('click', 'button', function(event) {
	event.preventDefault();
	let full = dataTable.row($(this).parents('tr')).data();
	if($(this).data('btn')=='btnSitios'){
		modalSitios(full);
	}
});

$(".actionBtnNew").click(function(){
	modalSitios();
})



$("#btnBuscar").click(function(){
	//load('on');
	dataTable.ajax.reload(null, false);
});
$("#btnLimpiar").click(function(){
	//load('on');
	$("#nombre").val('');
	$("#tipo").val('');
	$("#region").val('');
	$("#general").val('');
	dataTable.ajax.reload(null, false);
});

function modalSitios(full=false){
	let template = document.getElementById('tmpFormulario').innerHTML;
	let rendered = Mustache.render(template, {
		id 					: full.id_sitio_memoria,
		nombre				: full.nombre,
		codigo				: full.codigo,
		anio_creacion		: full.anio_creacion,
		fuente				: full.fuente,
		tipo				: full.tipo,
		estado				: full.estado_actual,
		sitio_web			: full.sitio_web,
		creado_por			: full.institucion_creacion,
		encargado			: full.institucion_cargo,
		contacto			: full.contacto,
		archivos_guardado	: full.archivos_guardados,
		razon_creacion		: full.razon_creacion,
		descripcion_fisica	: full.descripcion_fisica,
		region				: full.region,
		provincia			: full.provincia,
		distrito			: full.distrito,
		ubicacion			: full.ubicacion_exacta,
		ubicacion_map		: full.ubicacion_exacta_maps,
		ubicacion_iframe	: full.ubicacion_iframe,
		foto 				: (full.img_ubicacion)?`${base_url}${full.img_ubicacion}`:full.img_ubicacion,
		referencia_uso		: full.referencia_uso,
		name_foto			: full.img_ubicacion
	});
	console.log(full)

	BootstrapDialog.show({
		title: (full)?`Editar ${full.nombre}`:`Agregar Sitio`,
		message: rendered,
		size: BootstrapDialog.SIZE_WIDE,
		// draggable: true,
		buttons: [{
			label: "Cancelar",
			cssClass: 'btn btn-secondary rounded-0 mx-2',
			action: function(dialogRef) {
				dialogRef.close();
			}
		},{
			label: (full)?`Editar`:`Agregar`,
			cssClass: 'btn btn-primary rounded-0 mx-2',
			autospin: true,
			action: function(dialogRef) {
				let btnClick = this;

				$("#formSitios").validate({
					invalidHandler: function(event, validator) {
						button(dialogRef, btnClick, true);
					},
					submitHandler: function(form){
						let formData = new FormData(form);
						$.ajax({
							url: `${base_url}/sitios/agregar`,
							type: 'POST',
							data: formData,
							cache: false,
							contentType: false,
							processData: false
						})
						.done(function(response) {

							let res = JSON.parse(response);

							if (res.result == "OK") {
								msj(res.result, res.msj);
								dialogRef.close();
							}else{
								msj(res.result, res.msj);
								button(dialogRef, btnClick, true);
							}
						})
						.fail(function() {
							console.log('fail');
						})
						.always(function() {
							console.log("complete");
						});
						
						
						return false;
					}
					
				});
				
				$("#formSitios").submit();

				//btnClick.stopSpin();
			}
		}],
		onshown: function(dialogRef){
			let form =  dialogRef.getModalBody().find('form');

			//INPUT FILE IMG 
			form.find('#inputImgSitio').fileinput({
				theme: "fas",
				dropZoneEnabled: false,
				browseClass: "btn btn-secondary",
				browseLabel: "",
				browseIcon: '<i class="fas fa-upload"></i>',
				removeClass: "btn btn-danger",
				removeIcon: '<span class="fas fa-trash"></span>',
				msgPlaceholder: "Adjunta imagen (Máx. 10MB)",
				elErrorContainer: "#file-erro",
				msgErrorClass: 'invalid-feedback',
				showPreview: false,
				showUpload: false,
				allowedFileTypes: ['image'],
				allowedFileExtensions: ['png','jpg','jpeg'],
				language: 'es',
				maxFileCount: 1,
				focusCaptionOnBrowse: false,
				maxFileSize: 10000
			});

		

			form.fadeIn();

			$(".file-caption").click(function(){
				$(this).siblings('.input-group-btn').find('.btn-file').find('input').click();
			})

		},
		onhide: function(dialogRef){
			dataTable.ajax.reload(null, false);
		},
	});
}

function isUrl(s) {
	let response;
	var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
	if (regexp.test(s)) {
		response = s
	}else{
		response = regexp.test(s)
	}
	return response;
}
