$("#formLogin").validate({
	submitHandler: function (form) {
		var formData = new FormData(form);
		$.ajax({
			cache: false,
			contentType: false,
			processData: false,
			url: base_url+'Admin/login',
			type: 'POST',
			dataType: 'json',
			data: formData,
			beforeSend: function() {
				load('on');
			},
			success : function(){
				load('off');
			}
		})
		.done(function(response) {
			if (response.result == 'OK') {
				msj(response.result, response.msj);
				$("#formLogin")[0].reset();
				location.reload();
			}else{
				msj(response.result, response.msj);
			}
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}	
});


function modalRegistro(){
	
	let template = document.getElementById('tmpFormRegistro').innerHTML;
	let rendered = Mustache.render(template);

	BootstrapDialog.show({
		title:`REGISTRO`,
		message: rendered,
		size: BootstrapDialog.SIZE_WIDE,
		// draggable: true,
		buttons: [{
			label: `Enviar`,
			cssClass: 'btn btn-primary rounded-0 mx-2 mx-auto',
			autospin: true,
			action: function(dialogRef) {
				let btnClick = this;

				$("#frmRegistro").validate({
					invalidHandler: function(event, validator) {
						button(dialogRef, btnClick, true);
					},
					submitHandler: function(form){
						let formData = new FormData(form);
						$.ajax({
							url: `${base_url}/admin/registro`,
							type: 'POST',
							data: formData,
							cache: false,
							contentType: false,
							processData: false
						})
						.done(function(response) {

							let res = JSON.parse(response);

							if (res.result == "OK") {
								modalRegistroCompleto();
								dialogRef.close();
							}else{
								msj(res.result, res.msj);
								button(dialogRef, btnClick, true);
							}
						})
						.fail(function() {
							console.log('fail');
						})
						.always(function() {
							console.log("complete");
						});
						
						
						return false;
					}
					
				});
				
				$("#frmRegistro").submit();

				//btnClick.stopSpin();
			}
		}],
		onshown: function(dialogRef){
			

		},
		onhide: function(dialogRef){
			
		},
	});
}

function modalRegistroCompleto(){

	let template = document.getElementById('tmpCompletoRegistro').innerHTML;
	let rendered = Mustache.render(template);

	BootstrapDialog.show({
		title:``,
		message: rendered,
		size: BootstrapDialog.SIZE_WIDE,
		// draggable: true,
		
		onshown: function(dialogRef){
			setTimeout(function(){
				dialogRef.close();
			}, 1000);

		},
		onhide: function(dialogRef){
			
		},
	});
}


function modalLogin(){
	
	let template = document.getElementById('tmpFormLogin').innerHTML;
	let rendered = Mustache.render(template);

	BootstrapDialog.show({
		title:`INICIAR SESION`,
		message: rendered,
		size: BootstrapDialog.SIZE_WIDE,
		// draggable: true,
		buttons: [{
			label: `Iniciar`,
			cssClass: 'btn btn-primary rounded-0 mx-2 mx-auto',
			autospin: true,
			action: function(dialogRef) {
				let btnClick = this;

				$("#frmLogin").validate({
					invalidHandler: function(event, validator) {
						button(dialogRef, btnClick, true);
					},
					submitHandler: function(form){
						let formData = new FormData(form);
						$.ajax({
							url: `${base_url}/admin/login`,
							type: 'POST',
							data: formData,
							cache: false,
							contentType: false,
							processData: false
						})
						.done(function(response) {
							
							let res = JSON.parse(response);
							

							if (res.result == "OK") {
								msj(res.result, res.msj);
								dialogRef.close();
								
								
								location.href =base_url+"admin/catalogo";
							}else{
								msj(res.result, res.msj);
								button(dialogRef, btnClick, true);
								
							}
						})
						.fail(function() {
							console.log('fail');
						})
						.always(function() {
							console.log("complete");
						});
						
						
						return false;
					}
					
				});
				
				$("#frmLogin").submit();

				//btnClick.stopSpin();
			}
		}],
		onshown: function(dialogRef){
			

		},
		onhide: function(dialogRef){
			
		},
	});
}